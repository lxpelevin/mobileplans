package ru.megafon.plan.repository;

import io.quarkus.arc.Priority;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import ru.megafon.plan.model.ServiceType;
import ru.megafon.plan.persitence.entity.PlanEntity;
import ru.megafon.plan.persitence.entity.ServicePackageEntity;
import ru.megafon.plan.persitence.repository.PlanRepository;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;
import java.util.List;

@Priority(1)
@Alternative
@ApplicationScoped
public class TestPlanRepository extends PlanRepository {

    @PostConstruct
    public void init() {
        persist(new PlanEntity()
                        .setName("testPlan1")
                        .setIsArchived(false)
                        .setServicePackages(List.of(
                                        new ServicePackageEntity()
                                                .setName("Data1")
                                                .setType(ServiceType.DATA)
                                                .setValue(0L)
                                        ,
                                        new ServicePackageEntity()
                                                .setName("Voice1")
                                                .setType(ServiceType.VOICE)
                                                .setValue(0L),
                                        new ServicePackageEntity()
                                                .setName("SMS1")
                                                .setType(ServiceType.SMS)
                                                .setValue(0L)

                                )
                        ),
                new PlanEntity()
                        .setName("testPlan2")
                        .setIsArchived(false)

                        .setServicePackages(List.of(
                                        new ServicePackageEntity()
                                                .setName("Data2")
                                                .setType(ServiceType.DATA)
                                                .setValue(100L),
                                        new ServicePackageEntity()
                                                .setName("Voice2")
                                                .setType(ServiceType.VOICE)
                                                .setValue(0L),
                                        new ServicePackageEntity()
                                                .setName("SMS2")
                                                .setType(ServiceType.SMS)
                                                .setValue(0L)

                                )
                        )
                ,
                new PlanEntity()
                        .setName("test3")
                        .setIsArchived(true)

                        .setServicePackages(List.of(
                                        new ServicePackageEntity()
                                                .setName("Data3")
                                                .setType(ServiceType.DATA)
                                                .setValue(0L),
                                        new ServicePackageEntity()
                                                .setName("Voice3")
                                                .setType(ServiceType.VOICE)
                                                .setValue(100L),
                                        new ServicePackageEntity()
                                                .setName("SMS3")
                                                .setType(ServiceType.SMS)
                                                .setValue(0L)

                                )
                        )
        );

    }
}

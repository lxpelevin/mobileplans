package ru.megafon.plan;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import ru.megafon.plan.model.Plan;
import ru.megafon.plan.model.ServiceType;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class PlanFetcherTest {
    @Test
    void allPlans() {
        final Response response = given()
                .contentType(ContentType.JSON)
                .body("{\"query\":\"{ all{ id,name}}\"}")
                .when()
                .post("/graphql")
                .then()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .response();

        final List<Plan> allPlans = response.jsonPath().getList("data.all", Plan.class);
        assertThat(allPlans)
                .isNotEmpty()
                .hasSize(3)
                .extracting(Plan::getName)
                .contains("testPlan1", "testPlan2", "test3");

    }

    @Test
    void getById() {
        final Response response = given()
                .contentType(ContentType.JSON)
                .body("{\"query\":\"{byId(id: 1){id}}\"}")
                .when()
                .post("/graphql")
                .then()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .response();

        final Plan plan = response.jsonPath().getObject("data.byId", Plan.class);
        assertThat(plan)
                .isNotNull()
                .extracting(Plan::getId)
                .isEqualTo(1L);


    }

    @Test
    void getByFilterName() {
        final Response response = given()
                .contentType(ContentType.JSON)
                .body("{\"query\" :\"{PlanByFilter(filter:{name:{operator:\\\"contains\\\",value:\\\"testPlan\\\"}}) {id,name}}\"} ")
                .when()
                .post("/graphql")
                .then()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .response();


        final List<Plan> allPlans = response.jsonPath().getList("data.PlanByFilter", Plan.class);
        assertThat(allPlans)
                .isNotEmpty()
                .hasSize(2)
                .extracting(Plan::getName)
                .contains("testPlan1", "testPlan2");
    }

    @Test
    void getByFilterIsArchived() {
        final Response response = given()
                .contentType(ContentType.JSON)
                .body("{\"query\" :\"{PlanByFilter(filter:{isArchived:{operator:\\\"is\\\",value:\\\"true\\\"}}) {id,name,isArchived}}\"} ")
                .when()
                .post("/graphql")
                .then()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .response();

        final List<Plan> allPlans = response.jsonPath().getList("data.PlanByFilter", Plan.class);
        assertThat(allPlans)
                .isNotEmpty()
                .hasSize(1)
                .allMatch(Plan::getIsArchived)
                .extracting(Plan::getName)
                .contains("test3");
    }

    @Test
    void getByFilterIsUnlimitedInternet() {
        final Response response = given()
                .contentType(ContentType.JSON)
                .body("{\"query\" :\"{PlanByFilter(filter:{isUnlimitedData:{operator:\\\"is\\\",value:\\\"true\\\"}}) {id,name,servicePackages{type,value}}}\"}")
                .when()
                .post("/graphql")
                .then()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .response();

        final List<Plan> allPlans = response.jsonPath().getList("data.PlanByFilter", Plan.class);
        assertThat(allPlans)
                .isNotEmpty()
                .hasSize(2)
                .extracting(Plan::getServicePackages)
                .allMatch(it -> it.stream().anyMatch(it1 -> it1.getType().equals(ServiceType.DATA) && it1.getValue().equals(0L)));
    }

    @Test
    void getByFilterIsUnlimitedVoice() {
        final Response response = given()
                .contentType(ContentType.JSON)
                .body("{\"query\" :\"{PlanByFilter(filter:{isUnlimitedVoice:{operator:\\\"is\\\",value:\\\"true\\\"}}) {id,name,servicePackages{type,value}}}\"}")
                .when()
                .post("/graphql")
                .then()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .response();

        final List<Plan> allPlans = response.jsonPath().getList("data.PlanByFilter", Plan.class);
        assertThat(allPlans)
                .isNotEmpty()
                .hasSize(2)
                .extracting(Plan::getServicePackages)
                .allMatch(it -> it.stream().anyMatch(it1 -> it1.getType().equals(ServiceType.VOICE) && it1.getValue().equals(0L)));
    }





}
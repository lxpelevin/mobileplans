package ru.megafon.plan;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import ru.megafon.plan.model.Plan;

import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class PlanMutationTest {


    @Test
    void mutation() {
        Long planId;
        Response response = given()
                .contentType(ContentType.JSON)
                .body("{\"query\":\"mutation{create (plan:{name:\\\"testCreate\\\",isArchived:false,servicePackages:[{name:\\\"testCreate\\\",type:DATA,value:0}]}) {id,name,createDate,servicePackages{id,name,type,value}}}\"}")
                .when()
                .post("/graphql")
                .then()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .response();

        Plan plan = response.jsonPath().getObject("data.create", Plan.class);
        assertThat(plan)
                .isNotNull()
                .matches(it -> it.getName().equals("testCreate") && it.getServicePackages().size() == 1);

        planId = plan.getId();

       response = given()
                .contentType(ContentType.JSON)
                .body(format("{\"query\":\"mutation{update(id:%s,plan:{name:\\\"newName\\\",isArchived:true}){id,name,createDate,updateDate,servicePackages{id,name,type,value}}}\"}", planId))
                .when()
                .post("/graphql")
                .then()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .response();

        plan = response.jsonPath().getObject("data.update", Plan.class);
        assertThat(plan)
                .isNotNull()
                .matches(it -> it.getId().equals(planId) && it.getName().equals("newName") && it.getServicePackages().isEmpty());


        response = given()
                .contentType(ContentType.JSON)
                .body(format("{\"query\":\"mutation{delete (id: %s){id,name,createDate,servicePackages{id,name,type,value}}}\"}", planId))
                .when()
                .post("/graphql")
                .then()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .response();

        plan = response.jsonPath().getObject("data.delete", Plan.class);
        assertThat(plan)
                .isNotNull()
                .matches(it -> it.getId().equals(planId));

    }

}

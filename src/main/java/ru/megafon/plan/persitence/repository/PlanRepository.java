package ru.megafon.plan.persitence.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import ru.megafon.plan.model.ServiceType;
import ru.megafon.plan.model.PlanFilter;
import ru.megafon.plan.persitence.entity.ServicePackageEntity;
import ru.megafon.plan.persitence.entity.PlanEntity;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.List;

@ApplicationScoped
public class PlanRepository implements PanacheRepository<PlanEntity> {

    @Inject
    EntityManager entityManager;
    public static final long UNLIMITED = 0;

    public List<PlanEntity> findByCriteria(PlanFilter planFilter) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<PlanEntity> criteriaQuery = cb.createQuery(PlanEntity.class);
        Root<PlanEntity> root = criteriaQuery.from(PlanEntity.class);
        var predicate = cb.isTrue(cb.literal(true));

        if (planFilter.getName() != null) {
            predicate = cb.and(predicate, planFilter.getName().generateCriteria(cb, root.get("name")));
        }
        if (planFilter.getIsArchived() != null) {
            predicate = cb.and(predicate, planFilter.getIsArchived().generateCriteria(cb, root.get("isArchived")));
        }

        if (planFilter.getIsUnlimitedData() != null || planFilter.getIsUnlimitedVoice() != null) {
            var spPredicate = cb.isTrue(cb.literal(true));
            Subquery<PlanEntity> subquery = criteriaQuery.subquery(PlanEntity.class);
            Root<ServicePackageEntity> sp = subquery.from(ServicePackageEntity.class);

            if (planFilter.getIsUnlimitedData() != null) {
                spPredicate = cb.and(spPredicate,
                        cb.and(cb.equal(sp.get("type"), ServiceType.DATA),
                                cb.equal(sp.get("value"), UNLIMITED)));
                subquery.select(sp.get("plan")).where(spPredicate);
                predicate = cb.and(predicate, cb.in(root).value(subquery));

            }
            if (planFilter.getIsUnlimitedVoice() != null) {
                spPredicate = cb.and(spPredicate,
                        cb.and(cb.equal(sp.get("type"), ServiceType.VOICE),
                                cb.equal(sp.get("value"), UNLIMITED)));
                subquery.select(sp.get("plan")).where(spPredicate);
                predicate = cb.and(predicate, cb.in(root).value(subquery));
            }
        }
        if (predicate != null)
            criteriaQuery.where(predicate);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }
}

package ru.megafon.plan.persitence.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import ru.megafon.plan.persitence.entity.ServicePackageEntity;

import javax.enterprise.context.ApplicationScoped;


@ApplicationScoped
public class ServicePackageRepository implements PanacheRepository<ServicePackageEntity> {

}

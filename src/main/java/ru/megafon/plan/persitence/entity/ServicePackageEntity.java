package ru.megafon.plan.persitence.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import ru.megafon.plan.model.ServiceType;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name = "service_package")
@Where(clause = "REMOVED=false")
@SQLDelete(sql ="UPDATE service_package SET removed = true WHERE id = ?")
@Accessors(chain = true)
public class ServicePackageEntity extends BaseEntity {

    @NotEmpty(message = "Title may not be empty")
    @Size(min = 1, max = 128)
    @Column(name = "NAME", nullable = false , length = 128)
    private String name;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "TYPE", nullable = false)
    @NotNull
    private ServiceType type;

    @NotNull
    @Column(name = "VAL", nullable = false)
    private Long value;



    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PLAN_ID", nullable = false)
    private PlanEntity plan;
}

package ru.megafon.plan.persitence.entity;

import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "plan")
@Where(clause = "REMOVED=false")
@SQLDelete(sql = "UPDATE plan SET removed = true WHERE id = ?")
@Accessors(chain = true)
public class PlanEntity extends BaseEntity {

    @NotEmpty
    @Size(min = 1, max = 128)
    @Column(name = "NAME", nullable = false, length = 128)
    private String name;

    @Column(name = "ARCHIVED", nullable = false, columnDefinition = "boolean default false")
    private Boolean isArchived;

    @ToString.Exclude
    @OneToMany(mappedBy = "plan", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<ServicePackageEntity> servicePackages = new ArrayList<>();

    public PlanEntity setServicePackages(List<ServicePackageEntity> servicePackages) {
        this.servicePackages.clear();
        this.servicePackages.addAll(Optional.ofNullable(servicePackages).orElse(new ArrayList<>()));
        if (servicePackages != null && !servicePackages.isEmpty()) {
            for (val sp : this.servicePackages) {
                sp.setPlan(this);
            }
        }
        return this;
    }
}

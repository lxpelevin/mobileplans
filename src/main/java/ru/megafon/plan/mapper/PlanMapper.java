package ru.megafon.plan.mapper;

import org.mapstruct.*;
import ru.megafon.plan.model.Plan;
import ru.megafon.plan.persitence.entity.PlanEntity;

import java.util.List;

@Mapper(componentModel = "cdi", uses = {ServicePackageMapper.class}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PlanMapper {
    @Named(value = "all")
    Plan toDto(PlanEntity entity);

    @Named(value = "all")
   // @Mapping(target = "servicePackages.plan",source = "dto")
    PlanEntity toEntity(Plan dto);

    @IterableMapping(qualifiedByName = "all")
    List<Plan> toDtoList(List<PlanEntity> entity);

    @Mapping(target = "id", ignore = true)
    void updateEntityFromDto(Plan dto, @MappingTarget PlanEntity entity);


}


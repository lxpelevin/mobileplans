package ru.megafon.plan.mapper;

import org.mapstruct.*;
import ru.megafon.plan.model.ServicePackage;
import ru.megafon.plan.persitence.entity.ServicePackageEntity;

import java.util.List;

@Mapper(componentModel = "cdi", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ServicePackageMapper {
    @Named(value = "all")
    ServicePackage toDto(ServicePackageEntity entity);

    @Named(value = "all")
    @Mapping(target = "plan", ignore = true)
    ServicePackageEntity toEntity(ServicePackage dto);

    @IterableMapping(qualifiedByName = "all")
    List<ServicePackage> toDtoList(List<ServicePackageEntity> entity);

    @IterableMapping(qualifiedByName = "all")
    List<ServicePackageEntity> toEntityList(List<ServicePackage> dto);

}

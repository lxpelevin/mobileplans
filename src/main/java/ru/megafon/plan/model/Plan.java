package ru.megafon.plan.model;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.*;
import org.hibernate.annotations.Where;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@RegisterForReflection

public class Plan extends BaseDto {

    @NotEmpty
    @Size(min = 1, max = 128)
    private String name;
    @NotNull
    private Boolean isArchived = false;
    private List<ServicePackage> servicePackages;
}

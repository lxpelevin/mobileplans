package ru.megafon.plan.model;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class BaseDto {
    private Long id;
    private OffsetDateTime createDate;
    private OffsetDateTime updateDate;

}

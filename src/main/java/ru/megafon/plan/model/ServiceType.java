package ru.megafon.plan.model;


public enum ServiceType {
    VOICE,
    SMS,
    DATA
}

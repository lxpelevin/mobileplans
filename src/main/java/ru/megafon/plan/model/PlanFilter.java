package ru.megafon.plan.model;

import lombok.Data;

@Data
public class PlanFilter {
    private FilterField name;
    private FilterField isArchived;
    private FilterField isUnlimitedVoice;
    private FilterField isUnlimitedData;
}

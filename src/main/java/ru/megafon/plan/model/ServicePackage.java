package ru.megafon.plan.model;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.DefaultValue;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@RegisterForReflection
public class ServicePackage extends BaseDto {
    @NotEmpty
    @Size(min = 1, max = 128)
    private String name;
    @NotNull
    private ServiceType type;
    @NotNull
    private Long value;
}

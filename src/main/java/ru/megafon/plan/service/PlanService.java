package ru.megafon.plan.service;

import lombok.val;
import ru.megafon.plan.model.Plan;
import ru.megafon.plan.model.PlanFilter;
import ru.megafon.plan.mapper.PlanMapper;
import ru.megafon.plan.persitence.entity.PlanEntity;
import ru.megafon.plan.persitence.repository.PlanRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class PlanService {


    @Inject
    PlanMapper planMapper;

    @Inject
    PlanRepository planRepository;

    @Transactional
    public Plan create(Plan plan) {
        val entity = planMapper.toEntity(plan);
        planRepository.persistAndFlush(entity);
        if (planRepository.isPersistent(entity)) {
            val out = planRepository.findByIdOptional(entity.getId()).orElseThrow(NotFoundException::new);

            return planMapper.toDto(out);
        } else {
            throw new PersistenceException();
        }
    }

    @Transactional
    public Plan updateById(Long id, Plan plan) {
        val entity = planRepository.findById(id);
        if (entity == null) {
            throw new WebApplicationException("Plan does not exist.", 404);
        }
        planMapper.updateEntityFromDto(plan, entity);
        planRepository.persistAndFlush(entity);
        if (planRepository.isPersistent(entity)) {
            val out = planRepository.findByIdOptional(entity.getId()).orElseThrow(NotFoundException::new);

            return planMapper.toDto(out);
        } else {
            throw new PersistenceException();
        }

    }


    @Transactional
    public Plan delete(Long id) {
        val deletedPlanEntity = planRepository.findById(id);
        boolean isEntityDeleted = planRepository.deleteById(id);
        if (!isEntityDeleted) {
            throw new WebApplicationException("Plan does not exist.", 404);
        }
        return planMapper.toDto(deletedPlanEntity);
    }

    @Transactional
    public List<Plan> findByCriteria(PlanFilter planFilter) {
        return planMapper.toDtoList(planRepository.findByCriteria(planFilter));
    }

    @Transactional
    public Plan findById(long id) {

        return planMapper.toDto(planRepository.findById(id));
    }

    public List<Plan> findAll() {
        val result = planRepository.findAll().stream().toList();
        return planMapper.toDtoList(result);
    }
}

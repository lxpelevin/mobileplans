package ru.megafon.plan.api;

import org.eclipse.microprofile.graphql.*;
import ru.megafon.plan.model.Plan;
import ru.megafon.plan.service.PlanService;

import javax.inject.Inject;
import javax.validation.Valid;

@GraphQLApi
public class PlanMutation {

    @Inject
    PlanService planService;


    @Mutation
    @Description("Create plan")
    public Plan create(@Valid Plan plan) {
        return planService.create(plan);

    }

    @Mutation
    @Description("Update plan")
    public Plan update(@Name("id") Long id, @Valid Plan plan) {
        return planService.updateById(id, plan);
    }

    @Mutation
    @Description("Delete plan")
    public Plan delete(@Name("id") long id) {
        return planService.delete(id);
    }
}

package ru.megafon.plan.api;

import org.eclipse.microprofile.graphql.*;
import ru.megafon.plan.model.Plan;
import ru.megafon.plan.model.PlanFilter;
import ru.megafon.plan.service.PlanService;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@GraphQLApi
public class PlanFetcher {

    @Inject
    PlanService planService;

    @Query
    @Description("Get all plans")
    @Transactional
    public List<Plan> getAll() {
        return planService.findAll();
    }

    @Query
    @Description("Get plan by Id")
    public Plan getById(@Name("id") long id) {

        return planService.findById(id);
    }

    @Query("PlanByFilter")
    @Description("Get plans by filter")
    public List<Plan> findWithFilter(@Name("filter") PlanFilter planFilter) {
        return planService.findByCriteria(planFilter);
    }
}
